//Loads the express module
const express = require('express');
const  exphbs = require('express-handlebars');
const { path } = require('express/lib/application');
const { contentDisposition } = require('express/lib/utils');
const app = express();

app.engine('.hbs', exphbs.engine({defaultLayout: 'main', extname: '.hbs'}));

app.use(express.static('public'));

// Setting template Engine
app.set('view engine', '.hbs');

// routes
app.get('/', (req, res) => {
    res.render('home');
});
app.get('/about-us', (req, res) => {
    res.render('about-us', {layout: 'content.hbs'});
});
app.get('/contact-us', (req, res) => {
    res.render('contact-us', {layout: 'content.hbs'})
});

// port where app is served
app.listen(3000, () => {
    console.log('The web server has started on port 3000');
});

